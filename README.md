**Frog Lays Rendering（フロッグ・レイズ・レンダリング）とは：**  
フロントエンド・エンジニアリングで使用するマークアップ言語やプログラミング言語の技術資料です。  

**URL：**  
http://freit.jp/Frog-Lays/  

**ブランチの説明：**  
**▪release**  
現在リリース中のソースコードはreleaseブランチ上で管理されています。  

**▪render-only-selected**  
左のサイドバーで選択したコンテンツだけが表示される仕様のパターンのテンプレートです。  

**▪resize-on-scroll**  
スクロールでヘッダーやレイアウトが変化する仕様のパターンのテンプレートです。  

**▪sidebar-design-pattern-A**  
サイドバーのアクティブやホーバーをデザインしました。（Aパターン）

**▪sidebar-design-pattern-B**  
サイドバーのアクティブやホーバーをデザインしました。（Bパターン）

**▪sidebar-design-pattern-C**  
サイドバーのアクティブやホーバーをデザインしました。（Cパターン）

**製作者：**  
松岡将  
shomatsuoka[at]gmail.com  
