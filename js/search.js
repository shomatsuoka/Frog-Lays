// 変数宣言
var $ajax_list = $('#ajax-list')
var $attributes_list = $('#attributes-list')
var $callbacksObject_list = $('#callbacksObject-list')
var $core_list = $('#core-list')
var $css_list = $('#css-list')
var $data_list = $('#data-list')
var $deferredObject_list = $('#deferredObject-list')
var $dimensions_list = $('#dimensions-list')
var $effects_list = $('#effects-list')
var $events_list = $('#events-list')
var $forms_list = $('#forms-list')
var $internals_list = $('#internals-list')
var $classAttribute_list = $('#classAttributeList')
var $clone_list = $('#clone-list')
var $domInsertionAround_list = $('#domInsertionAround-list')
var $domInsertionInside_list = $('#domInsertionInside-list')
var $domInsertionOutside_list = $('#domInsertionOutside-list')
var $domRemoval_list = $('#domRemoval-list')
var $domReplacement_list = $('#domReplacement-list')
var $generalAttributes_list = $('#generalAttributes-list')
var $styleProperties_list = $('#styleProperties-list')
var $miscellaneous_list = $('#miscellaneous-list')
var $offsetList_list = $('#offset-list')
var $properties_list = $('#properties-list')
var $selectors_list = $('#selectors-list')
var $traversiong_list = $('#traversing-list')
var $utilities_list = $('#utilities-list')
var $domInsertionInside_wrap = $('#methods-domInsertionInside-wrap')
var $domInsertionOutside_wrap = $('#methods-domInsertionOutside-wrap')
var $domInsertionInside_header = $('#domInsertionInside-header')
var $domInsertionOutside_header = $('#domInsertionOutside-header')
var $domInsertionInside_sidebarHeader = $('#domInsertionInside-sidebar-header')
var $domInsertionInside_sidebarHeader_arrowIconImg = $('#domInsertionInside-sidebar-header').find('.arrow-icon img')
var $domInsertionOutside_sidebarHeader = $('#domInsertionOutside-sidebar-header')
var $domInsertionOutside_sidebarHeader_arrowIconImg = $('#domInsertionOutside-sidebar-header').find('.arrow-icon img')
var $search_input = $('#searchInput')
var $searchResultNumb = $('#searchResultNumb')
var $searchResult = $('#searchResult')
var $wrap = $('#wrap')
var $jquery_sidebarMajor = $('#jquery-sidebar-major')
var $manipulation_sidebarMajor = $('#manipulation-sidebar-major')
var $htmlSampleWrap = $('#html-sample-wrap')

// 初期表示
$(function () {
  // 起動時のレンダリングを隠すフェード
  $wrap.addClass('fadeOnLoad')

  // 検索結果数の非表示
  $searchResult.hide()

  // リストの表示
  for (var uniqueID_side in domInsertionInside) {
    $domInsertionInside_list.append(domInsertionInside[uniqueID_side])
  }
  for (var uniqueID_side in domInsertionOutside) {
    $domInsertionOutside_list.append(domInsertionOutside[uniqueID_side])
  }
  renderSelected()
})

// メインコンテンツの表示
for (var uniqueID_main in mainContents_domInsertionInside) {
  $domInsertionInside_wrap.append(mainContents_domInsertionInside[uniqueID_main] + hr)
}
for (var uniqueID_main in mainContents_domInsertionOutside) {
  $domInsertionOutside_wrap.append(mainContents_domInsertionOutside[uniqueID_main] + hr)
}

// 抽出
$search_input.on('keyup', function (e) {
  // DOM Insertion, Inside
  // appendで何度も表示されるため、一旦リセットする。
  $domInsertionInside_list.empty()
  $domInsertionOutside_list.empty()
  $domInsertionInside_wrap.empty()
  $domInsertionOutside_wrap.empty()
  runSamples()
  rdSrc()

  for (var uniqueID_side in domInsertionInside) {
    var matchResult = domInsertionInside[uniqueID_side].indexOf($('#searchInput').val())
    if (matchResult !== -1) {
      // 入力時に、入力欄の文字がひとつでもあったら、あった分だけ表示する。
      $domInsertionInside_list.append(domInsertionInside[uniqueID_side])
      for (var uniqueID_main in mainContents_domInsertionInside) {
        // 入力時に、サイドリストのidと同じメインコンテンツを表示する。
        if (uniqueID_main === uniqueID_side) {
          $domInsertionInside_wrap.append(mainContents_domInsertionInside[uniqueID_main] + hr)
        }
      }
    }
  }

  for (var uniqueID_side in domInsertionOutside) {
    var matchResult = domInsertionOutside[uniqueID_side].indexOf($('#searchInput').val())
    if (matchResult !== -1) {
      // 入力時に、入力欄の文字がひとつでもあったら、あった分だけ表示する。
      $domInsertionOutside_list.append(domInsertionOutside[uniqueID_side])
      for (var uniqueID_main in mainContents_domInsertionOutside) {
        // 入力時に、サイドリストのidと同じメインコンテンツを表示する。
        if (uniqueID_main === uniqueID_side) {
          $domInsertionOutside_wrap.append(mainContents_domInsertionOutside[uniqueID_main] + hr)
        }
      }
    }
  }

  // 1/2 ここに抽出機能をサイドバーのグループごとに追加
  // 入力時にリストをスライドダウンする。
  $('.sidebar>ul>li').next().find('ul').slideDown()
  // 入力時にスライドダウンするときに、アイコンを回転させる。
  if ($('.sidebar>ul>li').find('.arrow-icon img').hasClass('rotateBack')) {
    $(this).find('.arrow-icon img').toggleClass('rotate')
    $(this).find('.arrow-icon img').toggleClass('rotateBack')
  }

  if ($search_input.val() === '') {
    $domInsertionInside_list.empty()
    $domInsertionOutside_list.empty()
    $domInsertionInside_wrap.empty()
    $domInsertionOutside_wrap.empty()
    // 2/2 ここにもサイドバーのリストの全て表示をグループごとに追加
    // 入力時に入力が空だったら、サイドバーのリストをすべて表示する。
    for (var uniqueID_side in domInsertionInside) {
      $domInsertionInside_list.append(domInsertionInside[uniqueID_side])
      // 入力時に入力が空だったら、サイドバーのidと同じ分だけ表示する。
      for (var uniqueID_main in mainContents_domInsertionInside) {
        if (uniqueID_main === uniqueID_side) {
          $domInsertionInside_wrap.append(mainContents_domInsertionInside[uniqueID_main] + hr)
        }
      }
      for (var uniqueID_main in mainContents_domInsertionOutside) {
        if (uniqueID_main === uniqueID_side) {
          $domInsertionOutside_wrap.append(mainContents_domInsertionOutside[uniqueID_main] + hr)
        }
      }
    }
    for (var uniqueID_side in domInsertionOutside) {
      $domInsertionOutside_list.append(domInsertionOutside[uniqueID_side])
      // 入力時に入力が空だったら、サイドバーのidと同じ分だけ表示する。
      for (var uniqueID_main in mainContents_domInsertionInside) {
        if (uniqueID_main === uniqueID_side) {
          $domInsertionInside_wrap.append(mainContents_domInsertionInside[uniqueID_main] + hr)
        }
      }
      for (var uniqueID_main in mainContents_domInsertionOutside) {
        if (uniqueID_main === uniqueID_side) {
          $domInsertionOutside_wrap.append(mainContents_domInsertionOutside[uniqueID_main] + hr)
        }
      }
    }
  }

  renderSelected()
  ctrlMainHeadersRendering()

  // サイドバーのDOM Insertion, Outsideのリストに何も表示できなかったら、「DOM Insertion, Outside」の表題も消す。
  if ($domInsertionOutside_list.text() === '') {
    $domInsertionOutside_sidebarHeader.hide()
  } else {
    $domInsertionOutside_sidebarHeader.show()
  }

  // サイドバーのDOM Insertion, Insideのリストに何も表示できなかったら、「DOM Insertion, Inside」の表題も消す。
  if ($domInsertionInside_list.text() === '') {
    $domInsertionInside_sidebarHeader.hide()
  } else {
    $domInsertionInside_sidebarHeader.show()
  }

  // サイドバーのDOM Insertion, InsideもDom Insertion Outsideも表示できなかったら、「Manipulation」の表題を消す。
  if ($domInsertionInside_list.text() === '' && $domInsertionOutside_list.text() === '') {
    $manipulation_sidebarMajor.hide()
    $jquery_sidebarMajor.hide()
  } else {
    $manipulation_sidebarMajor.show()
    $jquery_sidebarMajor.show()
  }

  // リストにひとつでもひっかかったら、アイコンを下向きにする。
  if ($domInsertionInside_list.text() !== '' && $domInsertionInside_sidebarHeader_arrowIconImg.hasClass('rotateBack')) {
    $domInsertionInside_sidebarHeader_arrowIconImg.addClass('rotate')
    $domInsertionInside_sidebarHeader_arrowIconImg.removeClass('rotateBack')
  }

  if ($domInsertionOutside_list.text() !== '' && $domInsertionOutside_sidebarHeader_arrowIconImg.hasClass('rotateBack')) {
    $domInsertionOutside_sidebarHeader_arrowIconImg.addClass('rotate')
    $domInsertionOutside_sidebarHeader_arrowIconImg.removeClass('rotateBack')
  }

  // 検索結果の数字を表示する。
  $searchResultNumb.html($('#domInsertionInside-list li').length + $('#domInsertionOutside-list li').length)

  // 検索窓が空欄だったら検索結果数も表示しない
  if ($('#searchInput').val() === '' && e.keyCode === 8 || e.keyCode === 27 || e.keyCode === 17) {
    $searchResult.fadeOut()
  } else {
      $searchResult.fadeIn()
  }
})

// サイドバーのアイコン部分をクリックしたとき
$('.sidebar>ul>li').click(function() {
  $(this).next().find('ul').slideToggle()
  $(this).find('.arrow-icon img').toggleClass('rotate')
  $(this).find('.arrow-icon img').toggleClass('rotateBack')
})

function renderSelected () {
  // クリックしたサイドバーのリストを表示する
  $('li.sidebar-list li').click(function () {

    // $domInsertionInside_wrap.empty()
    // $domInsertionOutside_wrap.empty()

    if ($(this).text() === '.html()') {
      $('html, body').animate({
        scrollTop: $('#html-sample-wrap').offset().top + -160
      }, 600, 'swing')
      // $domInsertionInside_wrap.append(mainContents_domInsertionInside['10204'])
    }

    if ($(this).text() === '.append()') {
      $('html, body').animate({
        scrollTop: $('#append-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionInside_wrap.append(mainContents_domInsertionInside['11601'])
    }

    if ($(this).text() === '.appendTo()') {
      $('html, body').animate({
        scrollTop: $('#appendTo-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionInside_wrap.append(mainContents_domInsertionInside['11602'])
    }

    if ($(this).text() === '.prepend()') {
      $('html, body').animate({
        scrollTop: $('#prepend-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionInside_wrap.append(mainContents_domInsertionInside['11604'])
    }


    if ($(this).text() === '.prependTo()') {
      $('html, body').animate({
        scrollTop: $('#prependTo-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionInside_wrap.append(mainContents_domInsertionInside['11605'])
    }

    if ($(this).text() === '.text()') {
      $('html, body').animate({
        scrollTop: $('#text-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionInside_wrap.append(mainContents_domInsertionInside['11606'])
    }


    if ($(this).text() === '.after()') {
      $('html, body').animate({
        scrollTop: $('#after-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionOutside_wrap.append(mainContents_domInsertionOutside['11701'])
    }

    if ($(this).text() === '.before()') {
      $('html, body').animate({
        scrollTop: $('#before-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionOutside_wrap.append(mainContents_domInsertionOutside['11702'])
    }

    if ($(this).text() === '.insertAfter()') {
      $('html, body').animate({
        scrollTop: $('#insertAfter-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionOutside_wrap.append(mainContents_domInsertionOutside['11703'])
    }

    if ($(this).text() === '.insertBefore()') {
      $('html, body').animate({
        scrollTop: $('#insertBefore-sample-wrap').offset().top + -120
      }, 600, 'swing')
      // $domInsertionOutside_wrap.append(mainContents_domInsertionOutside['11704'])
    }

    ctrlMainHeadersRendering()
  })
}

// 表題の内容が表示してなかったら、表題も表示しない。
function ctrlMainHeadersRendering () {
  if ($domInsertionOutside_wrap.text() === '') {
    $domInsertionOutside_header.hide()
  } else {
    $domInsertionOutside_header.show()
  }

  if ($domInsertionInside_wrap.text() === '') {
    $domInsertionInside_header.hide()
  } else {
    $domInsertionInside_header.show()
  }
}
