var hr = '<div class="hr">' + '<img src="svg/spade-icon.svg">' + '</div>'

var mainContents_domInsertionInside = {
  // 11601 append()
  11601:
  '<article id="append-sample-wrap" class="methodWrap">' +
    '<h4>.append()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div id="append-sample-outside">' +
        '<div id="append-sample" class="viewBox-inline-flex clickable">' +
          '<p class="says-click">CLICK</p>' +
        '</div>' +
      '</div>' +
      '<div id="append-sample-reset">' +
        '<div class="viewBox-inline-flex clickable-reset">' +
          '<p class="says-reset">RESET</p>' +
        '</div>' +
      '</div>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' + '<code class="prettyprint lang-html">' +
        '<div id="rd-append-src-html"></div>' +
      '</code>' + '</pre>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' + '<code class="prettyprint lang-js">' +
        '<div id="rd-append-src-js"></div>' +
      '</code>' + '</pre>' +
    '</div>' +
  '</article>',

  // 11602 appendTo()
  11602:
  '<article id="appendTo-sample-wrap" class="methodWrap">' +
    '<h4>.appendTo()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div id="appendTo-sample-outside">' +
        '<div id="appendTo-sample" class="viewBox-inline-flex clickable">' +
          '<p class="says-click">CLICK</p>' +
        '</div>' +
      '</div>' +

      '<div id="appendTo-sample-reset">' +
        '<div class="viewBox-inline-flex clickable-reset">' +
          '<p class="says-reset">RESET</p>' +
        '</div>' +
      '</div>' +
    '</div>' +

    // code html
    '<div class="codeWrap">' +
      '<pre>' + '<code class="prettyprint lang-js">' +
        '<div id="rd-appendTo-src-html"></div>' +
      '</code>' + '</pre>' +
    '</div>' +

    // code js
    '<div class="codeWrap">' +
      '<pre>' + '<code class="prettyprint lang-js">' +
        '<div id="rd-appendTo-src-js"></div>' +
      '</code>' + '</pre>' +
    '</div>' +
  '</article>',

  // 10204 html()
  10204:
  '<article id="html-sample-wrap" class="methodWrap">' +
    '<h4>.html()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div class="viewBox-flex">' +
        '<div id="html-sample"></div>' +
      '</div>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-html">' +
          '<div id="rd-html-src-html"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-js">' +
          '<div id="rd-html-src-js"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +
  '</article>',

  // 11604 prepend()
  11604:
  '<article id="prepend-sample-wrap" class="methodWrap">' +
    '<h4>.prepend()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div id="prepend-sample-outside">' +
        '<div id="prepend-sample" class="viewBox-inline-flex clickable">' +
          '<p class="says-click">CLICK</p>' +
        '</div>' +
      '</div>' +
      '<div id="prepend-sample-reset">' +
        '<div class="viewBox-inline-flex clickable-reset">' +
          '<p class="says-reset">RESET</p>' +
        '</div>' +
      '</div>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-html">' +
          '<div id="rd-prepend-src-html"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-js">' +
          '<div id="rd-prepend-src-js"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

  '</article>',

  // 11605 prependTo()
  11605:
  '<article id="prependTo-sample-wrap" class="methodWrap">' +
    '<h4>.prependTo()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div id="prependTo-sample-outside">' +
        '<div id="prependTo-sample" class="viewBox-inline-flex clickable">' +
          '<p class="says-click">CLICK</p>' +
        '</div>' +
      '</div>' +
      '<div id="prependTo-sample-reset">' +
        '<div class="viewBox-inline-flex clickable-reset">' +
          '<p class="says-reset">RESET</p>' +
        '</div>' +
      '</div>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-html">' +
          '<div id="rd-prependTo-src-html"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-js">' +
          '<div id="rd-prependTo-src-js"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

  '</article>',

  // 11606 text()
  11606:
  '<article id="text-sample-wrap" class="methodWrap">' +
    '<h4>.text()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div class="viewBox-flex">' +
        '<p id="text-sample"></p>' +
      '</div>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-html">' +
          '<div id="rd-text-src-html"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-js">' +
          '<div id="rd-text-src-js"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +
  '</article>'

}

var mainContents_domInsertionOutside = {
// 11701 after()
  // 11701:
  // '<article id="after-sample-wrap" class="methodWrap">' +
  //   '<h4>.after()</h4>' +
  //   '<div class="viewBoxWrap">' +
  //     '<div id="after-sample-outside">' +
  //       '<div id="after-sample" class="viewBox-inline-flex clickable">' +
  //         '<p class="says-click">CLICK</p>' +
  //       '</div>' +
  //     '</div>' +
  //     '<div id="after-sample-reset">' +
  //       '<div class="viewBox-inline-flex clickable-reset">' +
  //         '<p class="says-reset">RESET</p>' +
  //       '</div>' +
  //     '</div>' +
  //   '</div>' +
  //
  //   '<div class="codeWrap">' +
  //     '<pre>' +
  //       '<code class="prettyprint lang-html">' +
  //         '<div id="rd-after-src-html"></div>' +
  //       '</code>' +
  //     '</pre>' +
  //   '</div>' +
  //
  //   '<div class="codeWrap">' +
  //     '<pre>' +
  //       '<code class="prettyprint lang-js">' +
  //         '<div id="rd-after-src-js"></div>' +
  //       '</code>' +
  //     '</pre>' +
  //   '</div>' +
  // '</article>',

  // 11702 before()
  11702:
  '<article id="before-sample-wrap" class="methodWrap">' +
    '<h4>.before()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div id="before-sample-outside">' +
        '<div id="before-sample" class="viewBox-inline-flex clickable">' +
          '<p class="says-click">CLICK</p>' +
        '</div>' +
      '</div>' +
      '<div id="before-sample-reset">' +
        '<div class="viewBox-inline-flex clickable-reset">' +
          '<p class="says-reset">RESET</p>' +
        '</div>' +
      '</div>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-html">' +
          '<div id="rd-before-src-html"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-js">' +
          '<div id="rd-before-src-js"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +
  '</article>',

  // 11703 insertAfter()
  // 11703:
  // '<article id="insertAfter-sample-wrap" class="methodWrap">' +
  //   '<h4>.insertAfter()</h4>' +
  //   '<div class="viewBoxWrap">' +
  //     '<div id="insertAfter-sample-outside">' +
  //       '<div id="insertAfter-sample" class="viewBox-inline-flex clickable">' +
  //         '<p class="says-click">CLICK</p>' +
  //       '</div>' +
  //     '</div>' +
  //     '<div id="insertAfter-sample-reset">' +
  //       '<div class="viewBox-inline-flex clickable-reset">' +
  //         '<p class="says-reset">RESET</p>' +
  //       '</div>' +
  //     '</div>' +
  //   '</div>' +
  //
  //   '<div class="codeWrap">' +
  //     '<pre>' +
  //       '<code class="prettyprint lang-html">' +
  //         '<div id="rd-insertAfter-src-html"></div>' +
  //       '</code>' +
  //     '</pre>' +
  //   '</div>' +
  //
  //   '<div class="codeWrap">' +
  //     '<pre>' +
  //       '<code class="prettyprint lang-js">' +
  //         '<div id="rd-insertAfter-src-js"></div>' +
  //       '</code>' +
  //     '</pre>' +
  //   '</div>' +
  // '</article>',

  // 11704 insertBefore()
  11704:
  '<article id="insertBefore-sample-wrap" class="methodWrap">' +
    '<h4>.insertBefore()</h4>' +
    '<div class="viewBoxWrap">' +
      '<div id="insertBefore-sample-outside">' +
        '<div id="insertBefore-sample" class="viewBox-inline-flex clickable">' +
          '<p class="says-click">CLICK</p>' +
        '</div>' +
      '</div>' +
      '<div id="insertBefore-sample-reset">' +
        '<div class="viewBox-inline-flex clickable-reset">' +
          '<p class="says-reset">RESET</p>' +
        '</div>' +
      '</div>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-html">' +
          '<div id="rd-insertBefore-src-html"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +

    '<div class="codeWrap">' +
      '<pre>' +
        '<code class="prettyprint lang-js">' +
          '<div id="rd-insertBefore-src-js"></div>' +
        '</code>' +
      '</pre>' +
    '</div>' +
  '</article>'
}

runSamples()

function runSamples () {
  $(function () {
    // 10204 html()
    $('#html-sample').html('<span>HTML</span>')

    // 11606 text()
    $('#text-sample').text('TEXT')

    // 11601 append()
    $('#append-sample').click(function () {
      $('#append-sample-outside').append('<div class="viewBox-inline-flex append-sample-copy">' + '<p>APPEND</p>' + '</div>')
      $('#append-sample-reset').fadeIn()
    })

    $('#append-sample-reset').click(function () {
      $('.append-sample-copy').fadeOut()
      $('#append-sample-reset').fadeOut()
    })

    // 11602 appendTo()
    $('#appendTo-sample').click(function () {
      $('<div class="viewBox-inline-flex appendTo-sample-copy">' + '<p>APPEND</p>' + '</div>').appendTo('#appendTo-sample-outside')
      $('#appendTo-sample-reset').fadeIn()
    })

    $('#appendTo-sample-reset').click(function () {
      $('.appendTo-sample-copy').fadeOut()
      $('#appendTo-sample-reset').fadeOut()
    })

    // 11604 prepend()
    $('#prepend-sample').click(function () {
      $('#prepend-sample-outside').prepend('<div class="viewBox-inline-flex prepend-sample-copy">' + '<p>PREPEND</p>' + '</div>')
      $('#prepend-sample-reset').fadeIn()
    })

    $('#prepend-sample-reset').click(function () {
      $('.prepend-sample-copy').fadeOut()
      $('#prepend-sample-reset').fadeOut()
    })

    // 11605 prependTo()
    $('#prependTo-sample').click(function () {
      $('<div class="viewBox-inline-flex prependTo-sample-copy">' + '<p>PREPEND</p>' + '</div>').prependTo('#prependTo-sample-outside')
      $('#prependTo-sample-reset').fadeIn()
    })

    $('#prependTo-sample-reset').click(function () {
      $('.prependTo-sample-copy').fadeOut()
      $('#prependTo-sample-reset').fadeOut()
    })

    // 11701 after()
    $('#after-sample').click(function () {
      $('#after-sample-outside').after('<div class="viewBox-inline-flex after-sample-copy">' + '<p>AFTER</p>' + '</div>')
      $('#after-sample-reset').fadeIn()
    })

    $('#after-sample-reset').click(function () {
      $('.after-sample-copy').fadeOut()
      $('#after-sample-reset').fadeOut()
    })

    // 11702 before()
    $('#before-sample').click(function () {
      $('#before-sample-outside').before('<div class="viewBox-inline-flex before-sample-copy">' + '<p>BEFORE</p>' + '</div>')
      // $('#before-sample-outside').css('margin-top', '12px')
      $('#before-sample-reset').fadeIn()
    })

    $('#before-sample-reset').click(function () {
      setTimeout(function () {
        $('#before-sample-outside').css('margin-top', '0')
      }, 400)
      $('.before-sample-copy').fadeOut(400)
      $('#before-sample-reset').fadeOut(400)
    })

    // 11703 insertBefore()
    $('#insertBefore-sample').click(function () {
      $('<div class="viewBox-inline-flex insertBefore-sample-copy">' + '<p>INSERT<br>BEFORE</p>' + '</div>').insertBefore('#insertBefore-sample-outside')
      // $('#insertBefore-sample-outside').css('margin-top', '12px')
      $('#insertBefore-sample-reset').fadeIn()
    })

    $('#insertBefore-sample-reset').click(function () {
      setTimeout(function () {
        $('#insertBefore-sample-outside').css('margin-top', '0')
      }, 400)
      $('.insertBefore-sample-copy').fadeOut(400)
      $('#insertBefore-sample-reset').fadeOut(400)
    })

    // 11704 insertAfter()
    $('#insertAfter-sample').click(function () {
      $('<div class="viewBox-inline-flex insertAfter-sample-copy">' + '<p>INSERT<br>AFTER</p>' + '</div>').insertAfter('#insertAfter-sample-outside')
      $('#insertAfter-sample-reset').fadeIn()
    })

    $('#insertAfter-sample-reset').click(function () {
      $('.insertAfter-sample-copy').fadeOut()
      $('#insertAfter-sample-reset').fadeOut()
    })
  })
}
