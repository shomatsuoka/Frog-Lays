rdSrc()

function rdSrc () {
  // 11601 append
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/append-html.txt',
      success: function (data) {
        $('#rd-append-src-html').text(data)
      }
    })

    $.ajax({
      url: '../Frog-Lays/txt/append-js.txt',
      success: function (data) {
        $('#rd-append-src-js').text(data)
      }
    })
  })

  // 11602 appendTo()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/appendTo-html.txt',
      success: function (data) {
        $('#rd-appendTo-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/appendTo-js.txt',
      success: function (data) {
        $('#rd-appendTo-src-js').text(data)
      }
    })
  })

  // 10204 html()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/html-html.txt',
      success: function (data) {
        $('#rd-html-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/html-js.txt',
      success: function (data) {
        $('#rd-html-src-js').text(data)
      }
    })
  })

  // 11604 prepend()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/prepend-html.txt',
      success: function (data) {
        $('#rd-prepend-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/prepend-js.txt',
      success: function (data) {
        $('#rd-prepend-src-js').text(data)
      }
    })
  })

  // 11605 prependTo()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/prependTo-html.txt',
      success: function (data) {
        $('#rd-prependTo-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/prependTo-js.txt',
      success: function (data) {
        $('#rd-prependTo-src-js').text(data)
      }
    })
  })

  // 11606 text()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/text-html.txt',
      success: function (data) {
        $('#rd-text-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/text-js.txt',
      success: function (data) {
        $('#rd-text-src-js').text(data)
      }
    })
  })

  // 11701 after()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/after-html.txt',
      success: function (data) {
        $('#rd-after-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/after-js.txt',
      success: function (data) {
        $('#rd-after-src-js').text(data)
      }
    })
  })

  // 11702 before()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/before-html.txt',
      success: function (data) {
        $('#rd-before-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/before-js.txt',
      success: function (data) {
        $('#rd-before-src-js').text(data)
      }
    })
  })

  // 11703 insertBefore()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/insertBefore-html.txt',
      success: function (data) {
        $('#rd-insertBefore-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/insertBefore-js.txt',
      success: function (data) {
        $('#rd-insertBefore-src-js').text(data)
      }
    })
  })

  // 11704 insertAfter()
  $(function () {
    $.ajax({
      url: '../Frog-Lays/txt/insertAfter-html.txt',
      success: function (data) {
        $('#rd-insertAfter-src-html').text(data)
      }
    })
    $.ajax({
      url: '../Frog-Lays/txt/insertAfter-js.txt',
      success: function (data) {
        $('#rd-insertAfter-src-js').text(data)
      }
    })
  })

}
