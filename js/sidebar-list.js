var ajax = {
  10101: '<li>.ajaxComplete()</li>',
  10102: '<li>.ajaxError()</li>',
  10103: '<li>.ajaxSend()</li>',
  10104: '<li>.ajaxStart()</li>',
  10105: '<li>.ajaxStop()</li>',
  10106: '<li>.ajaxSuccess()</li>',
  10107: '<li>jQuery.ajax()</li>',
  10108: '<li>jQuery.ajaxPrefilter()</li>',
  10109: '<li>jQuery.ajaxSetup()</li>',
  10110: '<li>jQuery.ajaxTransport()</li>',
  10111: '<li>jQuery.get()</li>',
  10112: '<li>jQuery.getJSON()</li>',
  10113: '<li>jQuery.getScript()</li>',
  10114: '<li>jQuery.param()</li>',
  10115: '<li>jQuery.post()</li>',
  10116: '<li>.load()*サポート終了(1.8)</li>',
  10117: '<li>.serialize()</li>',
  10118: '<li>.serializeArray()</li>'
}

var attributes = {
  10201: '<li>.addClass()</li>',
  10202: '<li>.attr()</li>',
  10203: '<li>.hasClass()</li>',
  10204: '<li>.html()</li>',
  10205: '<li>.prop()</li>',
  10206: '<li>.removeAttr()</li>',
  10207: '<li>.removeClass()</li>',
  10208: '<li>.removeProp()</li>',
  10209: '<li>.toggleClass()</li>',
  10210: '<li>.val()</li>'
}

var callbacksObject = {
  10301: '<li>callbacks.add()</li>',
  10302: '<li>callbacks.disable()</li>',
  10303: '<li>callbacks.disabled()</li>',
  10304: '<li>callbacks.empty()</li>',
  10305: '<li>callbacks.fire()</li>',
  10306: '<li>callbacks.fired()</li>',
  10307: '<li>callbacks.fireWith()</li>',
  10308: '<li>callbacks.has()</li>',
  10309: '<li>callbacks.lock()</li>',
  10310: '<li>callbacks.locked()</li>',
  10311: '<li>callbacks.remove()</li>',
  10312: '<li>jQuery.Callbacks()</li>'
}

var core = {
  10401: '<li>jQuery()</li>',
  10402: '<li>jQuery.holdReady()</li>',
  10403: '<li>jQuery.noConflict()</li>',
  10404: '<li>jQuery.ready',
  10405: '<li>jQuery.readyException()</li>',
  10406: '<li>jQuery.sub()*サポート終了(1.7)</li>',
  10407: '<li>jQuery.when()</li>'
}

var css = {
  10201: '<li>.addClass()</li>',
  10502: '<li>.css()</li>',
  10203: '<li>.hasClass()</li>',
  10504: '<li>.height()</li>',
  10505: '<li>.innerHeight()</li>',
  10506: '<li>.innerWidth()</li>',
  10507: '<li>jQuery.cssHooks',
  10508: '<li>jQuery.cssNumber',
  10509: '<li>jQuery.escapeSelector()</li>',
  10510: '<li>.offset()</li>',
  10511: '<li>.outerHeight()</li>',
  10512: '<li>.outerWidth()</li>',
  10513: '<li>.position()</li>',
  10207: '<li>.removeClass()</li>',
  10515: '<li>.scrollLeft()</li>',
  10516: '<li>.scrollTop()</li>',
  10209: '<li>.toggleClass()</li>',
  10518: '<li>.width()</li>'
}

var data = {
  10601: '<li>.clearQueue()</li>',
  10602: '<li>.data()</li>',
  10603: '<li>.dequeue()</li>',
  10604: '<li>jQuery.data()</li>',
  10605: '<li>jQuery.dequeue()</li>',
  10606: '<li>jQuery.hasData()</li>',
  10607: '<li>jQuery.queue()</li>',
  10608: '<li>jQuery.removeData()</li>',
  10609: '<li>.queue()</li>',
  10610: '<li>.removeData()</li>'
}

var deferredObject = {
  10701: '<li>deferred.always()</li>',
  10702: '<li>deferred.catch()</li>',
  10703: '<li>deferred.done()</li>',
  10704: '<li>deferred.fail()</li>',
  10705: '<li>deferred.isRejected()*サポート終了(1.7)</li>',
  10706: '<li>deferred.isResolved()*サポート終了(1.7)</li>',
  10707: '<li>deferred.notify()</li>',
  10708: '<li>deferred.notifyWith()</li>',
  10709: '<li>deferred.pipe()*非推奨(1.8)</li>',
  10710: '<li>deferred.progress()</li>',
  10711: '<li>deferred.promise()</li>',
  10712: '<li>deferred.reject()</li>',
  10713: '<li>deferred.rejectWith()</li>',
  10714: '<li>deferred.resolve()</li>',
  10715: '<li>deferred.resolveWith()</li>',
  10716: '<li>deferred.state()</li>',
  10717: '<li>deferred.then()</li>',
  10718: '<li>jQuery.Deferred()</li>',
  10407: '<li>jQuery.when()</li>',
  10720: '<li>.promise()</li>'
}

var dimensions = {
  10504: '<li>.height()</li>',
  10505: '<li>.innerHeight()</li>',
  10506: '<li>.innerWidth()</li>',
  10511: '<li>.outerHeight()</li>',
  10512: '<li>.outerWidth()</li>',
  10518: '<li>.width()</li>'
}

var effects = {
  10901: '<li>.animate()</li>',
  10601: '<li>.clearQueue()</li>',
  10903: '<li>.delay()</li>',
  10603: '<li>.dequeue()</li>',
  10905: '<li>.fadeIn()</li>',
  10906: '<li>.fadeOut()</li>',
  10907: '<li>.fadeTo()</li>',
  10908: '<li>.fadeToggle()</li>',
  10909: '<li>.finish()</li>',
  10910: '<li>.hide()</li>',
  10911: '<li>jQuery.fx.interval*非推奨(3.0)</li>',
  10912: '<li>jQuery.fx.off',
  10913: '<li>jQuery.speed',
  10609: '<li>.queue()</li>',
  10915: '<li>.show()</li>',
  10916: '<li>.slideDown()</li>',
  10917: '<li>.slideToggle()</li>',
  10918: '<li>.slideUp()</li>',
  10919: '<li>.stop()</li>',
  10920: '<li>.toggle()*サポート終了(1.8)</li>'
}

var events = {
  11001: '<li>.bind()*非推奨(3.0)</li>',
  11002: '<li>.blur()</li>',
  11003: '<li>.change()</li>',
  11004: '<li>.click()</li>',
  11005: '<li>.context*サポート終了(1.10)</li>',
  11006: '<li>.dblclick()</li>',
  11007: '<li>.delegate()*非推奨(3.0)</li>',
  11008: '<li>.die()*サポート終了(1.7)</li>',
  11009: '<li>.error()*サポート終了(1.8)</li>',
  11010: '<li>event.currentTarget',
  11011: '<li>event.data',
  11012: '<li>event.delegateTarget',
  11013: '<li>event.isDefaultPrevented()</li>',
  11014: '<li>event.isImmediatePropagationStopped()</li>',
  11015: '<li>event.isPropagationStopped()</li>',
  11016: '<li>event.metaKey',
  11017: '<li>event.namespace',
  11018: '<li>event.pageX',
  11019: '<li>event.pageY',
  11020: '<li>event.preventDefault()</li>',
  11021: '<li>event.relatedTarget',
  11022: '<li>event.result',
  11023: '<li>event.stopImmediatePropagation()</li>',
  11024: '<li>event.stopPropagation()</li>',
  11025: '<li>event.target',
  11026: '<li>event.timeStamp',
  11027: '<li>event.type',
  11028: '<li>event.which',
  11029: '<li>.focus()</li>',
  11030: '<li>.focusin()</li>',
  11031: '<li>.focusout()</li>',
  11032: '<li>.hover()</li>',
  10402: '<li>jQuery.holdReady()</li>',
  11034: '<li>jQuery.proxy()</li>',
  10404: '<li>jQuery.ready',
  11036: '<li>.keydown()</li>',
  11037: '<li>.keypress()</li>',
  11038: '<li>.keyup()</li>',
  11039: '<li>.live()*サポート終了(1.7)</li>',
  10116: '<li>.load()*サポート終了(1.8)</li>',
  11041: '<li>.mousedown()</li>',
  11042: '<li>.mouseenter()</li>',
  11043: '<li>.mouseleave()</li>',
  11044: '<li>.mousemove()</li>',
  11045: '<li>.mouseout',
  11046: '<li>.mouseover',
  11047: '<li>.mouseup',
  11048: '<li>.off()</li>',
  11049: '<li>.on()</li>',
  11050: '<li>.one()</li>',
  11051: '<li>.ready()</li>',
  11052: '<li>.resize()</li>',
  11053: '<li>.scroll()</li>',
  11054: '<li>.select()</li>',
  11055: '<li>.submit()</li>',
  10920: '<li>.toggle()*サポート終了(1.8)</li>',
  11057: '<li>.trigger()</li>',
  11058: '<li>.triggerHandler()</li>',
  11059: '<li>.unbind()*非推奨(3.0)</li>',
  11060: '<li>.undelegate()*非推奨(3.0)</li>',
  11061: '<li>.unload()*サポート終了(1.8)</li>'
}

var forms = {
  11002: '<li>.blur()</li>',
  11003: '<li>.change()</li>',
  11029: '<li>.focus()</li>',
  11104: '<li>.focusin',
  11031: '<li>.focusout()</li>',
  10114: '<li>jQuery.param()</li>',
  11054: '<li>.select()</li>',
  10117: '<li>.serialize()</li>',
  10118: '<li>.serializeArray()</li>',
  11055: '<li>.submit()</li>',
  10210: '<li>.val()</li>'
}

var internals = {
  11005: '<li>.context*サポート終了(1.10)</li>',
  11202: '<li>.jquery',
  11203: '<li>jQuery.error()</li>',
  11204: '<li>.pushStack()</li>',
  11205: '<li>.selector*サポート終了(1.7)</li>'
}

var classAttribute = {
  10201: '<li>.addClass()</li>',
  10203: '<li>.hasClass()</li>',
  10207: '<li>.removeClass()</li>',
  10209: '<li>.toggleClass()</li>'
}

var copying = {
  11401: '<li>.clone()</li>'
}

var domInsertionAround = {
  11501: '<li>.unwrap()</li>',
  11502: '<li>.wrap()</li>',
  11503: '<li>.wrapAll()</li>',
  11504: '<li>.wrapInner()</li>'
}

var domInsertionInside = {
  10204: '<li>.html()</li>',
  11606: '<li>.text()</li>',
  11601: '<li>.append()</li>',
  11602: '<li>.appendTo()</li>',
  11604: '<li>.prepend()</li>',
  11605: '<li>.prependTo()</li>'
}

var domInsertionOutside = {
  // 11701: '<li>.after()</li>',
  11702: '<li>.before()</li>',
  // 11703: '<li>.insertAfter()</li>',
  11704: '<li>.insertBefore()</li>'
}

var domRemoval = {
  11801: '<li>.detach()</li>',
  11802: '<li>.empty()</li>',
  11803: '<li>.remove()</li>',
  11501: '<li>.unwrap()</li>'
}

var domReplacement = {
  11901: '<li>.replaceAll()</li>',
  11902: '<li>.replaceWith()</li>'
}

var generalAttributes = {
  10202: '<li>.attr()</li>',
  10205: '<li>.prop()</li>',
  10206: '<li>.removeAttr()</li>',
  10208: '<li>.removeProp()</li>',
  10210: '<li>.val()</li>'
}

var styleProperties = {
  10502: '<li>.css()</li>',
  10504: '<li>.height()</li>',
  10505: '<li>.innerHeight()</li>',
  10506: '<li>.innerWidth()</li>',
  10508: '<li>jQuery.cssNumber',
  10510: '<li>.offset()</li>',
  10511: '<li>.outerHeight()</li>',
  10512: '<li>.outerWidth()</li>',
  10513: '<li>.position()</li>',
  10515: '<li>.scrollLeft()</li>',
  10516: '<li>.scrollTop()</li>',
  10518: '<li>.width()</li>'
}

var others = {
  12201: '<li>jQuery.htmlPrefilter()</li>'
}

var miscellaneous = {
  10602: '<li>.data()</li>',
  12302: '<li>.each()</li>',
  12303: '<li>.get()</li>',
  12304: '<li>.index()</li>',
  10403: '<li>jQuery.noConflict()</li>',
  10114: '<li>jQuery.param()</li>',
  10610: '<li>.removeData()</li>',
  12308: '<li>.size()*サポート終了(1.8)</li>',
  12309: '<li>.toArray()</li>'
}

var offset = {
  10510: '<li>.offset()</li>',
  12402: '<li>.offsetParent()</li>',
  10513: '<li>.position()</li>',
  10515: '<li>.scrollLeft()</li>',
  10516: '<li>.scrollTop()</li>'
}

var properties = {
  11005: '<li>.context*サポート終了(1.10)</li>',
  11202: '<li>.jquery',
  12503: '<li>jQuery.browser*サポート終了(1.3)</li>',
  10911: '<li>jQuery.fx.interval*非推奨(3.0)</li>',
  10912: '<li>jQuery.fx.off',
  10402: '<li>jQuery.holdReady()</li>',
  10404: '<li>jQuery.ready',
  12508: '<li>jQuery.support*非推奨(1.9)</li>',
  12509: '<li>.length',
  11205: '<li>.selector*サポート終了(1.7)</li>'
}

var selectors = {
  12601: '<li>AllSelector(“*”)</li>',
  12602: '<li>:animatedSelector',
  12603: '<li>AttributeContainsPrefixSelector[name|=”value”]',
  12604: '<li>AttributeContainsSelector[name*=”value”]',
  12605: '<li>AttributeContainsWordSelector[name~=”value”]',
  12606: '<li>AttributeEndsWithSelector[name$=”value”]',
  12607: '<li>AttributeEqualsSelector[name=”value”]',
  12608: '<li>AttributeNotEqualSelector[name!=”value”]',
  12609: '<li>AttributeStartsWithSelector[name^=”value”]',
  12610: '<li>:buttonSelector',
  12611: '<li>:checkboxSelector',
  12612: '<li>:checkedSelector',
  12613: '<li>ChildSelector(“parent>child”)</li>',
  12614: '<li>ClassSelector(“.class”)</li>',
  12615: '<li>:contains()Selector',
  12616: '<li>DescendantSelector(“ancestordescendant”)</li>',
  12617: '<li>:disabledSelector',
  12618: '<li>ElementSelector(“element”)</li>',
  12619: '<li>:emptySelector',
  12620: '<li>:enabledSelector',
  12621: '<li>:eq()Selector',
  12622: '<li>:evenSelector',
  12623: '<li>:fileSelector',
  12624: '<li>:first-childSelector',
  12625: '<li>:first-of-typeSelector',
  12626: '<li>:firstSelector',
  12627: '<li>:focusSelector',
  12628: '<li>:gt()Selector',
  12629: '<li>HasAttributeSelector[name]',
  12630: '<li>:has()Selector',
  12631: '<li>:headerSelector',
  12632: '<li>:hiddenSelector',
  12633: '<li>IDSelector(“#id”)</li>',
  12634: '<li>:imageSelector',
  12635: '<li>:inputSelector',
  12636: '<li>:lang()Selector',
  12637: '<li>:last-childSelector',
  12638: '<li>:last-of-typeSelector',
  12639: '<li>:lastSelector',
  12640: '<li>:lt()Selector',
  12641: '<li>MultipleAttributeSelector[name=”value”][name2=”value2″]',
  12642: '<li>MultipleSelector(“selector1,selector2,selectorN”)</li>',
  12643: '<li>NextAdjacentSelector(“prev+next”)</li>',
  12644: '<li>NextSiblingsSelector(“prev~siblings”)</li>',
  12645: '<li>:not()Selector',
  12646: '<li>:nth-child()Selector',
  12647: '<li>:nth-last-child()Selector',
  12648: '<li>:nth-last-of-type()Selector',
  12649: '<li>:nth-of-type()Selector',
  12650: '<li>:oddSelector',
  12651: '<li>:only-childSelector',
  12652: '<li>:only-of-typeSelector',
  12653: '<li>:parentSelector',
  12654: '<li>:passwordSelector',
  12655: '<li>:radioSelector',
  12656: '<li>:resetSelector',
  12657: '<li>:rootSelector',
  12658: '<li>:selectedSelector',
  12659: '<li>:submitSelector',
  12660: '<li>:targetSelector',
  12661: '<li>:textSelector',
  12662: '<li>:visibleSelector'
}

var traversing = {
  12701: '<li>.add()</li>',
  12702: '<li>.addBack()</li>',
  12703: '<li>.andSelf()*サポート終了(1.8)</li>',
  12704: '<li>.children()</li>',
  12705: '<li>.closest()</li>',
  12706: '<li>.contents()</li>',
  12302: '<li>.each()</li>',
  12708: '<li>.end()</li>',
  12709: '<li>.eq()</li>',
  12710: '<li>.filter()</li>',
  12711: '<li>.find()</li>',
  12712: '<li>.first()</li>',
  12713: '<li>.has()</li>',
  12714: '<li>.is()</li>',
  12715: '<li>.last()</li>',
  12716: '<li>.map()</li>',
  12717: '<li>.next()</li>',
  12718: '<li>.nextAll()</li>',
  12719: '<li>.nextUntil()</li>',
  12720: '<li>.not()</li>',
  12402: '<li>.offsetParent()</li>',
  12722: '<li>.parent()</li>',
  12723: '<li>.parents()</li>',
  12724: '<li>.parentsUntil()</li>',
  12725: '<li>.prev()</li>',
  12726: '<li>.prevAll()</li>',
  12727: '<li>.prevUntil()</li>',
  12728: '<li>.siblings()</li>',
  12729: '<li>.slice()</li>'
}

var utilities = {
  10601: '<li>.clearQueue()</li>',
  10603: '<li>.dequeue()</li>',
  12803: '<li>jQuery.boxModel*サポート終了(1.3)</li>',
  12503: '<li>jQuery.browser*サポート終了(1.3)</li>',
  12805: '<li>jQuery.contains()</li>',
  10604: '<li>jQuery.data()</li>',
  10605: '<li>jQuery.dequeue()</li>',
  12808: '<li>jQuery.each()</li>',
  12809: '<li>jQuery.extend()</li>',
  12810: '<li>jQuery.fn.extend()</li>',
  12811: '<li>jQuery.globalEval()</li>',
  12812: '<li>jQuery.grep()</li>',
  12813: '<li>jQuery.inArray()</li>',
  12814: '<li>jQuery.isArray()</li>',
  12815: '<li>jQuery.isEmptyObject()</li>',
  12816: '<li>jQuery.isFunction()*非推奨（3.3）',
  12817: '<li>jQuery.isNumeric()</li>',
  12818: '<li>jQuery.isPlainObject()</li>',
  12819: '<li>jQuery.isWindow()*非推奨（3.3）',
  12820: '<li>jQuery.isXMLDoc()</li>',
  12821: '<li>jQuery.makeArray()</li>',
  12822: '<li>jQuery.map()</li>',
  12823: '<li>jQuery.merge()</li>',
  12824: '<li>jQuery.noop()</li>',
  12825: '<li>jQuery.now()</li>',
  12826: '<li>jQuery.parseHTML()</li>',
  12827: '<li>jQuery.parseJSON()</li>',
  12828: '<li>jQuery.parseXML()</li>',
  11034: '<li>jQuery.proxy()</li>',
  10607: '<li>jQuery.queue()</li>',
  10608: '<li>jQuery.removeData()</li>',
  12508: '<li>jQuery.support*非推奨（1.9）',
  12833: '<li>jQuery.trim()</li>',
  12834: '<li>jQuery.type()</li>',
  12835: '<li>jQuery.unique()</li>',
  12836: '<li>jQuery.uniqueSort()</li>',
  10609: '<li>.queue()</li>'
}
