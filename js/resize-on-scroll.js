$(window).scroll(function () {
  const scrollPosition = $(window).scrollTop()
  const ScrollPositionNumb = 200
  const $header = $('header')
  const $sidebar = $('.sidebar')
  const $contentsWrap = $('.contentsWrap')

  if ($(window).scrollTop() >= 200) {
    if ($header.hasClass('resize-header-larger')) {
      $header.removeClass('resize-header-larger')
      $header.addClass('resize-header-smaller')
      $('.arch').fadeIn()


    }
    if ($sidebar.hasClass('resize-sidebar-larger')) {
      $sidebar.removeClass('resize-sidebar-larger')
      $sidebar.addClass('resize-sidebar-smaller')
    }
    if ($contentsWrap.hasClass('resize-contents-larger')) {
      $contentsWrap.removeClass('resize-contents-larger')
      $contentsWrap.addClass('resize-contents-smaller')
    }
  } else if ($(window).scrollTop() < 200) {
    if ($header.hasClass('resize-header-smaller')) {
      $header.removeClass('resize-header-smaller')
      $header.addClass('resize-header-larger')
      $('.arch').fadeOut()
    }
    if ($sidebar.hasClass('resize-sidebar-smaller')) {
      $sidebar.removeClass('resize-sidebar-smaller')
      $sidebar.addClass('resize-sidebar-larger')
    }
    if ($contentsWrap.hasClass('resize-contents-smaller')) {
      $contentsWrap.removeClass('resize-contents-smaller')
      $contentsWrap.addClass('resize-contents-larger')
    }
  }
})
