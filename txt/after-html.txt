<div class="viewBoxWrap">
  <div id="after-sample-outside">
    <div id="after-sample" class="viewBox-inline-flex">
      <p class="says-click">CLICK</p>
    </div>
  </div>
  <div id="after-sample-reset">
    <div class="viewBox-inline-flex">
      <p class="says-reset">RESET</p>
    </div>
  </div>
</div>
